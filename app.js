// content of index.js
const bodyParser = require('body-parser');
const express = require('express');
const https = require('https');
const http = require('http');
const fs = require('fs');
const util = require('util');
const Particle = require('particle-api-js');
const ping_interval = 600000; // 10 minutes
const TC_Box = require('./traffic_box.js').TC_Box;

// data for configuring traffic boxes
var traffic_box_configurations = {
    'centennial_pioneer': { 'latitude': 44.05664,
                            'longitude': -123.023882,
                            'name': 'centennial_pioneer',
                            'particle_id': '24003f000b51343334363138',
                            'relays': [ 
                                        {'id': 1,
                                         'phase': 2,
                                         'name' : 'east',
                                         'ingress': 95.0,
                                         'egress': 95.0},
                                        {'id': 2,
                                         'phase': 4,
                                         'name': 'south',
                                         'ingress': 166.0,
                                         'egress' : 166.0},
                                        {'id': 3,
                                         'phase': 4,
                                         'name' : 'west',
                                         'ingress': 280.0,
                                         'egress': 280.0},
                                        {'id': 4,
                                         'name': 'north',
                                         'ingress': 345.0,
                                         'egress': 345.0}
                                      ]
                          }
};

// pki certificate for TSL
const options = {
    key: fs.readFileSync('/home/kerndtr/ks-fastraq-bike-key.pem'),
    cert: fs.readFileSync('/home/kerndtr/ks-fastraq-bike-chain.pem')
};

// load user id and password for particle login
var particle_credentials_file = fs.readFileSync("/src/urbanBiker/particle_credentials.json");
var particle_credentials = JSON.parse(particle_credentials_file);

// login to particle to obtain access token
var particle = new Particle;
var particle_token = null;

particle.login(particle_credentials).then(
        setup_electron_ping,
        function (err) {
            console.log('Particle login failed.', err);
        }
);

// setup interval timers to ping electrons
var ping_timers = [];

function setup_electron_ping(login_data) {
    particle_token = login_data.body.access_token;
    var devicesPr = particle.listDevices( {product: "5046", auth: particle_token} );
    let timestamp = new Date();
    devicesPr.then(
        function(data) {
            console.log("%s Retrieved list for %d devices", timestamp.toISOString(), data.body.devices.length);
            for (let device of data.body.devices) {
                var next_timer = setInterval(ping_device, ping_interval, device.id);
                ping_timers.push(next_timer);
		timestamp = new Date();
                console.log("%s Installing timer for device: %s", timestamp.toISOString(), device.id);
            }
        },
        function(err) {
            console.log("%s Failed to retrieve particle device list %s", timestamp.toISOString(),  err);
        }
    );
    init_traffic_lights();
}

function ping_device(device) {
    if (particle_token) {
        var mark = Date.now();
	let timestamp = new Date();
        var pingPr = particle.callFunction({ deviceId: device, name: 'request_ping',
                                             argument: '', product: 'fast-track-v300',
                                             auth: particle_token});
        pingPr.then(
            function(data) {
                let delta = Date.now() - mark;
                console.log("%s %s  acknowledged ping request in %d milliseconds", timestamp.toISOString(), device,  delta);
            },
            function(err) {
                console.log("%s Failed ping request to %s: %s", timestamp.toISOString(),  device, err);
            }
        );
    }
}


var app = express();
http.createServer(app).listen(80);
https.createServer(options, app).listen(443);

const valid_routes = ["/phase", "/log_state", "/log_gps"];

var users = {
	'user2': {
		'email': 'user2@cs.uoregon.edu',
		'first_name': 'first',
		'last_name': 'last',
		'password': 'pswd'
	},
	'user3': {
		'email': 'user3@cs.uoregon.edu',
		'first_name': 'first',
		'last_name': 'last',
		'password': 'pswd'
	},
	'user4': {
		'email': 'user4@cs.uoregon.edu',
		'first_name': 'first',
		'last_name': 'last',
		'password': 'pswd'
	}
};

var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('userdb.db');

db.serialize(function() {
	// Create table if it doesn't exist
	db.all("CREATE TABLE if not exists user_info (email TEXT, first_name TEXT, last_name TEXT, password TEXT)");
	
	// Select contents of table and check if they've been initialized
	// If not, initialize user data
	db.all("SELECT * FROM user_info", function(err, row) {
		if(err) {
			console.log(err);
		}
		else {
			if(row.length === 0) {
				console.log("Empty");
				var stmt = db.prepare("INSERT INTO user_info VALUES (?, ?, ?, ?)");
				for(var item in users) {
					stmt.run(users[item].email, users[item].first_name, users[item].last_name, users[item].password);
				}
				stmt.finalize();
			}
			db.each("SELECT email, first_name, last_name, password FROM user_info", function(err, row) {
				console.log(row.email + ", " + row.first_name + ", " + row.last_name + ", " + row.password);
			});
		}
	});
});

// Logging function to capture all requests and log according to route
// only log messages which are directed to known route.
// TODO: log other requests to an error file
function logger(req, res, next) {
    let time_stamp = req.requestTime.toISOString();
    let body_text = "";
    for (let idx in valid_routes) {
        if (req.originalUrl === valid_routes[idx]) {
            body_text = util.inspect(req.body);
            let msg = `"${time_stamp}" ${req.ip} ${req.method} ${req.originalUrl} "${body_text}"`;
            console.log(msg);
            break;
        }
   }
    next();
}

// Add received time to all requests
function request_time(req, res, next) {
    req.requestTime = new Date();
    next();
}

// object to hold all traffic light instances
var traffic_lights = {};
function init_traffic_lights() {
    for (let name in traffic_box_configurations) {
        let tl = traffic_box_configurations[name];
    	traffic_lights[name] = new TC_Box(tl.latitude, tl.longitude, tl.name, tl.particle_id);
    	traffic_lights[name].particle_token = particle_token;
    	//console.log("particle token (%s) set in %s (%s)", particle_token, name, traffic_lights[name].particle_token);
    	for (let rl of tl.relays) {
            traffic_lights[name].add_relay(rl.id, rl.phase, rl.name, rl.ingress, rl.egress);
    	}
    }
}

app.use(request_time);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(logger);

app.get("*", function (request, response) {
    response.send('You have reached ks.fastraq.bike');
});


app.post("/phase", function (request, response) {

	// JSON Object format:
	// { "latitude: N.NNN, 
        //   "trigger": ['on' | 'off'],
        //   "longitude: N.NNN,
        //   "mode" : ['bike' || 'auto' || 'ped'],
        //   "signal_light_name": <String>,
        //   "email": <email address>,
        //   "direction": [ -1, 0 ... 360 ]
        // }
	//

	db.all("SELECT * from user_info where email = '" + request.body.email + "'", function(err, row) {
		if(err) {
			console.log(err);
			response.send({ status: 'FAILED' });
		}
		else {
                        //TODO: fix user authentication. this isn't actucally
                        //      doing any authentication at all
                        
                        // pull out required attributes from request body
                        let trigger = null;
                        let mode = null;
                        let direction = null;
                        let signal_light_name = null;
                        let user = request.body.email;
                        if ( 'trigger' in request.body) {
                            for (let attr of ["off", "on"]) {
                                if (request.body.trigger === attr) {
                                trigger = request.body.trigger;
                                break;
                            }
                        }
                        if ('mode' in request.body) {
                            for (let attr of ["bike", "auto", "ped"]) {
                                if (request.body.mode === attr) {
                                    mode = request.body.mode;
                                    break;
                                }
                            }
                        }                        }
                        if ('direction' in request.body) {
                            direction = parseInt(request.body.direction);
                            if (isNaN(direction) || (direction < 0) || (direction > 360)) {
                                direction = null;
                            }
                        }
                        if ('signal_light_name' in request.body) {
                            for (let attr of ["centennial_pioneer"]) {
                                if (request.body.signal_light_name === attr) {
                                    signal_light_name = attr;
                                    break;
                                }
                            }
                       }
                        // return error if any null values remain
                        if ( !trigger || !mode || (direction === null) || !signal_light_name) {
                            response.status(404).send('Required attributes missing from body');
                            return;
                        }
			if (trigger === "on") {
				var requestType = 'relay_on';
			}
			else {
				var requestType = 'relay_off';
			}
                        // function call through particle cloud
                        if (signal_light_name in traffic_lights) {
                            traffic_lights[signal_light_name].request(user, direction, requestType);
                        }
			response.send({ status: 'SUCCESS' });
                }
	});
});

