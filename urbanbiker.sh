#!/bin/bash
#
# Rickie Kerndt <rkerndt@cs.uoregon.edu> 
# /etc/rc.d/init.d/urbanbiker
#
# system5 init script for urbanbiker startup and shutdown
#
#
# <tags
# chkconfig: 2345 20 80
# description: node.js service to relay urban biker traffic controller _
# requests to message broker>

# Source function library
. /etc/init.d/functions

export NODE_PATH=/usr/lib/node_modules
APP_JS=/src/urbanBiker/app.js
LOGFILE=/var/log/forever.log
OUTFILE=/var/log/urbanbiker.log
ERRFILE=/var/log/urbanbiker-error.log

start() {
    echo -n "Starting urbanbiker ks server: "
    forever -l${LOGFILE} -o${OUTFILE} -e${ERRFILE} --append start ${APP_JS}
    touch /var/lock/subsys/urbanbiker
}

stop() {
    echo -n "Shutting down urbanbiker ks server: "
    forever stop ${APP_JS}
    rm -f /var/lock/subsys/urbanbiker
}

restart() {
    echo -n "Restarting urbanbiker ks server: "
    forever -l${LOGFILE} -o${OUTFILE} -e${ERRFILE} --append restart ${APP_JS}
}

condrestart() {
    [ -f /var/lock/subsys/urbanbiker ] && restart || :
}

case "$1" in
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    reload)
        restart
        ;;
    condrestart)
        condrestart
        ;;
    *)
        echo "Usage: urbanbiker {start|stop|reload|restart|condrestart}"
        exit 1
        ;;
esac
exit $?
