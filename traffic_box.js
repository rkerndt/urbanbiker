/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

const Particle = require('particle-api-js');

function TC_Request(user, relay) {
    this.user = user;
    this.relay = relay;
    this.timestamp = Date.now();
};

function TC_Relay(id, phase, name, ingress, egress) {
    this.id = id;       // unique identifer (within a TC_box)
    this.phase = phase;
    this.name = name;
    this.ingress = ingress;
    this.egress = egress;
    this.count = 0;
    };

function TC_Box(latitude, longitude, name, particle_id) {
    this.latitude = latitude;
    this.longitude = longitude;
    this.name = name;
    this.particle = new Particle;
    this.particle_id = particle_id;
    this.particle_token = undefined;
    this.relays = [];
    this.requests = {};
    this.num_requests = 0;
    this.timer = undefined;
    this.margin = 45;
    this.max_timeout = 120000;
    this.product = 'fast-trackv300';
};

TC_Box.prototype = {
    request: function(user, heading, action) {
	let timestamp = new Date();
        console.log("%s %s received %s request from %s heading %s", timestamp.toISOString(), this.name, action, user, heading);
        let relay_id = 0;
        if (action === 'relay_on') {
            relay_id = this.select_relay(heading, 'ingress');
            if (! (user in this.requests)) {
                this.requests[user] = new TC_Request(user, relay_id);
		console.log("%s Added %s to %s for relay %d", timestamp.toISOString(), user, this.name, relay_id);
                this.send_relay(relay_id, action);
                this.relays[relay_id].count += 1;
		this.num_requests += 1;
            }
            else if (relay_id === this.requests[user].relay) {
		console.log("%s Extending %s on %s for relay %d", timestamp.toISOString(), user, this.name, relay_id);
                this.requests[user].timestamp = Date.now();
                this.send_relay(relay_id, action);
            }
        }
        else if (action === 'relay_off') {
            relay_id = this.select_relay(heading, 'egress');
            if (relay_id > 0) {
                if ((user in this.requests) && (this.requests[user].relay == relay_id)) {
                    console.log("%s Removing %s on %s", timestamp.toISOString(), user, this.name);
                    delete this.requests[user];
                    this.relays[relay_id].count -= 1;
		    this.num_requests -= 1;
		    console.log("%s Relay %d count=%d, number of requests = %d", timestamp.toISOString(), relay_id, this.relays[relay_id].count, this.num_requests);
                    if (this.relays[relay_id].count === 0) {
                        this.send_relay(relay_id, action);
                    }
                }
            }
        }
        if ((this.num_requests > 0) && (this.timer === undefined)) {
            // bind the purge function to this instance for use as callback
	    console.log("%s Installing timer for %s", timestamp.toISOString(), this.name);
            var bound_purge = this.purge.bind(this);
            this.timer = setInterval(bound_purge, this.max_timeout);
        }
        else if ((this.num_requests === 0) && (this.timer !== undefined)) {
	    console.log("%s Clearing timer for %s", timestamp.toISOString(), this.name);
            clearInterval(this.timer);
            this.timer = undefined;
        }
    },
    add_relay: function(id, phase, name, ingress, egress) {
        this.relays.push( new TC_Relay(id, phase, name, ingress, egress));
    },
    select_relay: function(heading, move) {
        let relay_id = 0;
        if (move === 'ingress' || move ==='egress') {
            for (let relay of this.relays) {
                let lower = relay[move] - this.margin;
                if (lower < 0) {
                    lower += 360;
                }
                let upper = (relay[move] + this.margin) % 360;
		//console.log("checking heading %d against relay %d ingress = %d margin=%d lower = %d, upper = %d", heading, relay.id, relay[move], this.margin, lower, upper);
                if ( heading >= lower && heading < upper) {
                    relay_id = relay.id;
                    break;
                }
            }
        }
        //console.log("%s matches relay %d to direction %s", this.name, relay_id, heading);
        return relay_id;
    },
    purge: function() {
        // removes users from requests object when they exceed the timeout for sending
        // an off request. this function does not however, send relay_off to the devices
        // we let the timeout mechanism on the device manage the timeouts.
	let timestamp = new Date();
        let mark = timestamp.getTime() - this.max_timeout;
        for (let user in this.requests) {
            if (this.requests[user].timestamp < mark) {
		console.log("%s Timed out %s on %s for relay %d", timestamp.toISOString(), user, this.name, this.requests[user].relay);
                let relay_id = this.requests[user].relay;
                delete this.requests[user];
                this.relays[relay_id].count -= 1;
            }
        }
    },
    send_relay: function(relay_id, action) {
        // function call through particle cloud
	//console.log("Requesting relay %d %s with token %s", relay_id, action, this.particle_token);
        let name = this.name;
        this.particle.callFunction({deviceId: this.particle_id, name: action, argument: String(relay_id),
            product: TC_Box.product, auth: this.particle_token}).then(
            function(data) {
                let timestamp = new Date();
                console.log("%s: %s acknowledged for relay %d %s", timestamp.toISOString(), name, relay_id, action);
            },
            function(err) {
                let timestamp = new Date();
                console.log("%s: Phase request failed for %s: %s", timestamp.toISOString(), name, err);
            });	
    }
};

exports.TC_Relay = TC_Relay;
exports.TC_Box = TC_Box;
